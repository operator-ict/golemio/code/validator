"use strict";

/**
 * Validates data (object, array of objects, object with sub-objects) according to a given schema.
 */
export interface IValidator {
    /**
     * Data validation - validates either an array of objects or a single object
     *
     * @param {any} data Data to be validated by the validator's schema
     * @returns {boolean} Returns true or throw error
     */
    Validate(data: any): Promise<boolean>;

    setLogger(logger: any): void;
}
