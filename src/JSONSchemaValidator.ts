"use strict";

import * as Ajv from "ajv";
import { IValidator } from "./";
import { ILogger, log } from "./Logger";

/**
 * JSON schema validator class. Validates data (object, array of objects, object with sub-objects)
 * according to a given json schema.
 */
export class JSONSchemaValidator implements IValidator {
    /** Name of the model */
    protected modelName: string;
    /** Reference to json schema object, used for validation */
    protected jsonSchema: object;
    /** The Ajv instance */
    protected ajv: Ajv.Ajv;
    protected strictValidator: JSONSchemaValidator | undefined;
    protected logger: ILogger;

    /**
     * @param modelName Name of the model
     * @param jsonSchema JSON Schema object by which to validate the input data
     * @param strict sets coerceTypes = false for Avj  ( https://ajv.js.org/#coercing-data-types )
     */

    constructor(modelName: string, jsonSchema: object, deprecated: boolean = false) {
        this.modelName = modelName;
        this.jsonSchema = jsonSchema;

        const options: Ajv.Options = { schemaId: "auto" };

        this.ajv = new Ajv(options);
        this.ajv.addMetaSchema(require("ajv/lib/refs/json-schema-draft-04.json"));
        this.logger = log;
    }

    /**
     * Data validation - validates input data exactly by json schema
     *
     * @param {any} data Data to be validated by the validator's schema
     * @returns {boolean} Returns true or throws error
     */
    public async Validate(data: any): Promise<boolean> {
        try {
            const validate = this.ajv.compile({
                $async: true,
                ...this.jsonSchema,
            });
            await validate(data);

            return true;
        } catch (error) {
            this.logger.debug(error);
            if (error instanceof Ajv.ValidationError) {
                throw new Error(`${error.message} ${JSON.stringify(error.errors)}`);
            } else {
                throw error;
            }
        }
    }

    public setLogger(logger: any) {
        this.logger = logger;
    }
}
