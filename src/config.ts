export default {
    colorful_logs: process.env.COLORFUL_LOGS,
    log_level: process.env.LOG_LEVEL,
};
