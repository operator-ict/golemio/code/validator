import * as debug from "debug";
import pino from "pino";
import config from "./config";

export type LoggerCustomLevels = "verbose" | "silly";
export type ILogger = pino.Logger<LoggerCustomLevels>;

const logger: ILogger = pino({
    level: config.log_level?.toLowerCase() || "info",
    customLevels: {
        silly: 5,
        verbose: 25,
    },
    base: undefined,
    formatters: {
        level: (label: string) => {
            return { level: label };
        },
    },
    errorKey: "error",
    messageKey: "message",
    timestamp: pino.stdTimeFunctions.isoTime,
});

const loggerDebugLog = logger.debug.bind(logger);
const loggerSillyLog = logger.silly.bind(logger);

const debugLog: debug.Debugger = debug("golemio:validator");

logger.silly = (...args: any): void => {
    debugLog.apply(null, args);
    loggerSillyLog.apply(null, args);
};

logger.debug = (...args: any): void => {
    debugLog.apply(null, args);
    loggerDebugLog.apply(null, args);
};

export { logger as log };
