"use strict";

import "mocha";
import { JSONSchemaValidator } from "../src/";

import * as chai from "chai";
import { expect } from "chai";
import * as chaiAsPromised from "chai-as-promised";

chai.use(chaiAsPromised);

describe("JSONSchemaValidator", () => {
    let validatorStrict: JSONSchemaValidator;
    let testSchema: object;
    let testData: any;

    beforeEach(() => {
        /* tslint:disable object-literal-sort-keys object-literal-key-quotes trailing-comma */
        testSchema = {
            type: "array",
            items: {
                type: "object",
                required: ["id", "num_of_free_places", "num_of_taken_places", "timestamp", "total_num_of_places"],
                properties: {
                    id: {
                        type: "integer",
                    },
                    num_of_free_places: {
                        type: "integer",
                    },
                    num_of_taken_places: {
                        type: "integer",
                    },
                    timestamp: {
                        type: "integer",
                    },
                    total_num_of_places: {
                        type: "integer",
                    },
                },
            },
        };
        /* tslint:enable */
        testData = [
            {
                id: 534001,
                num_of_free_places: 91,
                num_of_taken_places: 1,
                timestamp: 1545063078063,
                total_num_of_places: 92,
            },
            {
                id: 534015,
                num_of_free_places: 43,
                num_of_taken_places: 126,
                timestamp: 1545063078065,
                total_num_of_places: "169",
            },
        ];

        validatorStrict = new JSONSchemaValidator("Test", testSchema, true);
    });

    it("should has Validate method", async () => {
        expect(validatorStrict.Validate).not.to.be.undefined;
    });

    it("should should properly validates the valid data", async () => {
        await expect(validatorStrict.Validate([testData[0]])).to.be.fulfilled;
    });

    it("should should not cats and validate data in strict mode", async () => {
        await expect(validatorStrict.Validate(testData)).not.to.be.fulfilled;
    });

    it("should should validates empty data", async () => {
        await expect(validatorStrict.Validate([])).to.be.fulfilled;
    });

    it("should throw error if the data are not valid", async () => {
        delete testData[1].timestamp;
        await expect(validatorStrict.Validate(testData)).to.be.rejectedWith(Error);
    });
});
