# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [0.3.5] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [0.3.4] - 2024-01-22

### Changed

-   upgrade pino to 8.17.2 ([core#89](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/89))

## [0.3.3] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [0.3.2] - 2023-04-26

### Removed

-   Remove validations based on mongoose ([core#66](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/66))

## [0.3.1] - 2023-02-26

### Changed

-   Update Node.js to v18.14.0 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [0.3.0] - 2023-01-23

### Changed

-   Migrate to npm

## [0.2.5] - 2022-11-29

### Changed

-   use pino logger instead of winston

## [0.2.4] - 2022-09-01

### Changed

-   upgrade Mongoose from 5.6.7 to 6.5.3.

## [0.2.3] - 2022-07-18

### Removed

-   possibility to use coerce types settings

## [0.2.2] - 2022-05-02

### Added

-   Strict JSON Schema validator for non strict validations to find out which modules and libraries are dependent on coerceTypes option.

## [0.2.1] - 2020-08-20

-   initial

