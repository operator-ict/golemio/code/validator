# Golemio Validator Library

Library of Validator classes of the Golemio Data Platform System.

Developed by http://operatorict.cz

## Prerequisites

- node.js (https://nodejs.org)
- TypeScript (https://www.typescriptlang.org/)

## Installation

Install Node

Install all npm modules using command:
```
npm install
```

## Compilation of typescript code

To compile typescript code into js one-time

```
npm run build
```
or run this, to watch all changes
```
npm run build-watch
```
from the application's root directory.


## Usage

In your project's `package.json` set dependency to Validator
```
npm install @golemio/validator --save

or

yarn add @golemio/validator --save
```

Then import module, e.g.
```
import { Validator } from "@golemio/validator";
```

## Logging

Logging uses `pino` for standard logging with levels and `debug` (https://www.npmjs.com/package/debug) for debugging.

All logs with `silly` and `debug` level are printed as standard log (if appropriate log level is set) using `pino` as well as using `debug` module with `"golemio:validator"` settings.

You can set both `LOG_LEVEL` and `DEBUG` settings in ENV variables.

## Documentation

For generating documentation run `npm run generate-docs`. TypeDoc source code documentation is located in `docs/typedoc`.

## Contribution guidelines

Please read `CONTRIBUTING.md`.

## Troubleshooting

Contact benak@operatorict.cz or vycpalek@operatorict.cz
